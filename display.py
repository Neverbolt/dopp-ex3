#!/usr/bin/env python3

########################## vaccination differences over time #####################################################################################################

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import main as load

oecd = load.load_oecd()


def plot_biggest_diff(oecd):
    fig = plt.figure()
    gs = gridspec.GridSpec(ncols=1, nrows=2)

    i = 0
    for col in ["DTP", "MEASLES"]:
        biggest_diff = (oecd.groupby("iso3166")[col].max() - oecd.groupby("iso3166")[col].min()).sort_values(
            ascending=False)[:5]

        ax = fig.add_subplot(gs[i])
        i += 1
        for country, diff in biggest_diff.iteritems():
            ax.plot(oecd.loc[country][col], label=f"{country} (diff {diff})")

        ax.legend()

    fig.set_size_inches(10, 7)
    fig.set_dpi(100)
    fig.show()


def plot_vacc_diff(oecd):
    biggest_diff = (oecd.groupby("iso3166")["DTP"].sum() - oecd.groupby("iso3166")["MEASLES"].sum()).sort_values(
        ascending=False)[:4]

    fig = plt.figure()
    gs = gridspec.GridSpec(nrows=2, ncols=2)

    i = 0
    for country, diff in biggest_diff.iteritems():
        ax = fig.add_subplot(gs[i // 2, i % 2])
        i += 1

        ax.plot(oecd.loc[country]["DTP"], label=f"{country} DTP")
        ax.plot(oecd.loc[country]["MEASLES"], label=f"{country} MEASLES")
        ax.legend()

    fig.set_size_inches(10, 7)
    fig.set_dpi(100)
    fig.show()


plot_biggest_diff(oecd)

plot_vacc_diff(oecd)

########################## HEATMAP #####################################################################################################

import pandas as pd
import numpy as np

import matplotlib
from matplotlib import pyplot as plt

import main

df = main.load_all()


def heatmap(data, row_labels, col_labels, ax=None, cbar_kw={}, cbarlabel="", **kwargs):
    "taken from https://matplotlib.org/gallery/images_contours_and_fields/image_annotated_heatmap.html"

    if not ax:
        ax = plt.gca()
    im = ax.imshow(data, **kwargs)

    cbar = None
    if cbarlabel is not None:
        cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
        if "orientation" in cbar_kw:
            if cbar_kw["orientation"] == "vertical":
                cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")
            else:
                cbar.ax.set_ylabel(cbarlabel)
        else:
            cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    ax.tick_params(top=True, bottom=False, labeltop=True, labelbottom=False)
    plt.setp(ax.get_xticklabels(), rotation=-30, ha="right", rotation_mode="anchor")

    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1] + 1) - .5, minor=True)
    ax.set_yticks(np.arange(data.shape[0] + 1) - .5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=1)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar


def annotate_heatmap(im, data=None, valfmt="{x:.2f}", textcolors=["black", "white"], **textkw):
    "taken from https://matplotlib.org/gallery/images_contours_and_fields/image_annotated_heatmap.html"
    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    threshold = im.norm(data.max()) / 2.

    kw = dict(horizontalalignment="center", verticalalignment="center")
    kw.update(textkw)

    if isinstance(valfmt, str):
        valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)

    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)

    return texts


def show_heatmap(arr, row, col, name, label="correlation", cmap="YlGn", cbar_kw={}, textkw={}):
    fig, ax = plt.subplots()

    im, cbar = heatmap(arr, row, col, ax=ax, cmap=cmap, cbarlabel=label, cbar_kw=cbar_kw)
    texts = annotate_heatmap(im, **textkw)

    fig.set_size_inches(10, 10)
    fig.set_dpi(100)
    # fig.savefig(name+".png", bbox_inches="tight")

    plt.show()

show_heatmap(df, df.columns, df.columns, "correlations", textkw={"fontsize": "small"})


########################## correlating just religion #####################################################################################################

import main
df = main.load_all()
religions = ["christianity", "judaism", "islam", "buddhism", "zoroastrian", "hindu", "sikh", "shinto", "bahai", "taoism", "jain", "confucianism", "syncretic", "animist", "nonreligious", "otherreligions"]
non_religions = list(set(df.columns)-set(religions)-set(["region", "status"]))

show_heatmap(df.corr().abs().loc[religions][non_religions], religions, df.columns, "correlations", textkw={"fontsize": "small"})


########################## correlation of changes over time #####################################################################################################

import main
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

def interpolate_year(df, columns):
    df = df.reset_index()
    df["date"] = pd.to_datetime(df["year"], format="%Y")
    df = df.set_index("date")
    countries_with_religion = df[df[columns].notna().any(axis=1)]["iso3166"].unique()
    for country in countries_with_religion:
        df.loc[df["iso3166"].eq(country), columns] = df[df["iso3166"].eq(country)][columns].interpolate(method="time")

    df = df.reset_index()
    df = df.drop(columns=["date"])
    return df.set_index(["iso3166", "year"])


def fill_years(df):
    df = df[~df.index.duplicated(keep='first')]

    ind = []
    years = df.reset_index().set_index("iso3166").groupby(level=0).agg({"year": ["min","max"]})
    for entry in years.itertuples():
        ind.extend([(entry.Index, year) for year in range(entry._1, entry._2+1)])

    df.reindex(pd.MultiIndex.from_tuples(ind))

    df = df.reset_index()
    df["year"] = pd.to_datetime(df["year"], format="%Y")
    df = df.set_index(["iso3166", "year"])
    return df

changing_cols = pd.Index(['some', 'BCG', 'DIPHTERIA', 'HIB', 'MEASLES', 'PERTUSSIS', 'POLIO','TETANUS', 'Fertility', 'GDP', 'GDPC', 'LifeExpectancy','MortalityRate', 'pop0to14', 'pop15to24', 'pop25to64', 'pop64to99','popGrowth', 'hiv', 'ruralPerc', 'population','christianity', 'judaism', 'islam', 'buddhism','zoroastrian', 'hindu', 'sikh', 'shinto', 'bahai', 'taoism', 'jain','confucianism', 'syncretic', 'animist', 'nonreligious','otherreligions'], dtype='object')

df = main.load_all()
df = fill_years(df)

diffs = df[changing_cols].diff().corr().abs()

pctdiffs = df[changing_cols].pct_change().corr().abs()

show_heatmap(diffs, diffs.columns, diffs.columns, "correlations", textkw={"fontsize": "small"})
show_heatmap(pctdiffs, pctdiffs.columns, pctdiffs.columns, "correlations", textkw={"fontsize": "small"})



########################## improved country time correlation #####################################################################################################

import main
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

def correlate_over_time(df, title=""):
    changing_cols = pd.Index(['some', 'BCG', 'DIPHTERIA', 'HIB', 'MEASLES', 'PERTUSSIS', 'POLIO','TETANUS', 'Fertility', 'GDP', 'GDPC', 'LifeExpectancy','MortalityRate', 'pop0to14', 'pop15to24', 'pop25to64', 'pop64to99','popGrowth', 'hiv', 'ruralPerc', 'population','christianity', 'judaism', 'islam', 'buddhism','zoroastrian', 'hindu', 'sikh', 'shinto', 'bahai', 'taoism', 'jain','confucianism', 'syncretic', 'animist', 'nonreligious','otherreligions'], dtype='object')

    countmask = pd.DataFrame(index=changing_cols, columns=changing_cols).fillna(0)
    abssum = pd.DataFrame(index=changing_cols, columns=changing_cols).fillna(0)
    relsum = pd.DataFrame(index=changing_cols, columns=changing_cols).fillna(0)

    countries = df.reset_index()["iso3166"].unique()
    for c in countries:
        country = df.loc[c][changing_cols]
        norm = ((country-country.mean())/(country.max()-country.min()))

        diff = norm.diff().corr()
        countmask += diff.isna().replace(to_replace={True: 0, False: len(country)})
        diff = diff.fillna(0)
        relsum += diff*len(country)
        abssum += diff.abs()*len(country)

    countmask = countmask.replace(to_replace={0: np.nan})

    relsum /= countmask
    abssum /= countmask
    countmask /= countmask.max().max()

    relevant = countmask.sum(axis=1)>(countmask.sum(axis=1).max()/5)
    relsum = relsum[relevant][relsum.columns[relevant]]
    abssum = abssum[relevant][abssum.columns[relevant]]
    countmask = countmask[relevant][countmask.columns[relevant]]

    show_heatmap(relsum, relsum.columns, relsum.columns, title + " relative correlations", textkw={"fontsize": "xx-small"}, cmap="RdYlGn")
    show_heatmap(abssum, abssum.columns, abssum.columns, title + " absolute correlations", textkw={"fontsize":"xx-small"}, cmap="YlGn")
    show_heatmap(countmask, countmask.columns, countmask.columns, title + " data availability", textkw={"fontsize":"xx-small"}, cmap="YlGn")



df = main.load_all()
df = fill_years(df)
correlate_over_time(df, title="total")

df["status"].unique()
correlate_over_time(df[df["status"].eq("DEVELOPING")], "developing")
correlate_over_time(df[df["status"].eq("TRANSITIONING")], "transitioning")
correlate_over_time(df[df["status"].eq("DEVELOPED")], "developed")
