#!/usr/bin/python3
import re
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt


def load_iso3166():
    df = pd.read_csv('datasets/countrycodes.csv')
    df = df[['name', 'alpha-3', 'country-code']]
    df.columns = ['country', 'iso3166', 'country-code']
    return df


def load_dhs():
    df = pd.read_csv('datasets/DHS.csv')
    # take first year for every season (eg. for 2017-18, use 2017)
    df['year'] = df['Survey'].apply(lambda x: int(re.split(r'[ -]', x)[0]))
    # we only want one year olds
    df = df[['Country', 'year', *[col for col in df.columns if col.endswith('12-23')]]]
    df.columns = ['country', 'year', 'BCG', 'DPT1', 'DPT2', 'DPT3', 'POLIO0', 'POLIO1', 'POLIO2', 'POLIO3', 'MEASLES',
                  'all', 'none']
    cc = load_iso3166()
    df = pd.merge(df, cc, left_on='country', right_on='country')
    df.set_index(['iso3166', 'year'], inplace=True)
    return df.drop(columns='country').sort_index()


def load_oecd():
    df = pd.read_csv('datasets/OECD.csv')
    df.columns = ['iso3166', 'x', 'vaccine', 'x', 'x', 'year', 'value', 'x']
    df = df[['iso3166', 'vaccine', 'year', 'value']]
    df = df.pivot_table(index=['iso3166', 'year'], columns='vaccine', values='value')
    return df.sort_index()


def load_england():
    df = pd.read_csv('datasets/england.csv')
    # take first year for every season (eg. for 2017-18, use 2017)
    df['Year'] = df['Year'].apply(lambda x: int(x.split('-')[0]))
    df['iso3166'] = 'GBR'
    df.columns = ['year', 'DTPPH', 'DIPHTERIA', 'TETANUS', 'POLIO', 'PERTUSSIS', 'HIB', 'MMR', 'MENC', 'PCV', 'ROTA',
                  'MENB', 'iso3166']
    df.set_index(['iso3166', 'year'], inplace=True)
    return df.sort_index()


def england_processed():
    df = load_england().loc['GBR']
    df['DIPHTERIA'].fillna(df['DTPPH'], inplace=True)
    df['TETANUS'].fillna(df['DTPPH'], inplace=True)
    df['POLIO'].fillna(df['DTPPH'], inplace=True)
    df['PERTUSSIS'].fillna(df['DTPPH'], inplace=True)
    df['HIB'].fillna(df['DTPPH'], inplace=True)
    df.drop(columns=['MMR','DTPPH'], inplace=True)
    return df


def load_all(add_missing_rows=False):
    dhs = load_dhs()
    oecd = load_oecd()
    oecd.columns = ['DTP', 'MEASLES_']
    eng = load_england()

    df = pd.merge(dhs, oecd, left_index=True, right_index=True, how='outer')
    df = pd.merge(df, eng, left_index=True, right_index=True, how='outer')
    if add_missing_rows:
        df = fill_year(df)

    # take dhs data, and use oecd for missing values if they are present
    df['MEASLES'] = df['MEASLES'].fillna(df['MEASLES_'])

    # combination vaccines
    df['DIPHTERIA'] = df['DIPHTERIA'].fillna(df['DTP']).fillna(df['DPT3']).fillna(df['DTPPH'])
    df['PERTUSSIS'] = df['PERTUSSIS'].fillna(df['DPT3']).fillna(df['DTPPH'])
    df['TETANUS'] = df['TETANUS'].fillna(df['DPT3']).fillna(df['DTPPH'])
    df['POLIO'] = df['POLIO'].fillna(df['POLIO3']).fillna(df['DTP']).fillna(df['DTPPH'])
    df['HIB'] = df['HIB'].fillna(df['DTPPH'])
    df['MEASLES'] = df['MEASLES'].fillna(df['MMR'])

    # Simplify
    df = df[
        ['BCG', 'DIPHTERIA', 'HIB', 'MEASLES', 'MENB', 'MENC', 'MMR', 'PCV', 'PERTUSSIS', 'POLIO', 'ROTA', 'TETANUS',
         'none']]

    # How many have at least one vaccination
    # Use .2 quantile as approximation (some children
    # will not be vaccinated for vacc with min number,
    # but have another vaccination)
    df['some'] = (100 - df['none']).fillna(df.quantile(0.2, axis=1))

    # Filter out data only available for england (-> use england dataset if you need this)
    df = df[['some', 'BCG', 'DIPHTERIA', 'HIB', 'MEASLES', 'PERTUSSIS', 'POLIO', 'TETANUS']]

    dev = load_development_status().set_index('iso3166')['status']
    df = pd.merge(df, dev, left_index=True, right_index=True)

    cens = load_census()
    df = pd.merge(df, cens, left_index=True, right_index=True)

    misc_info = load_country_info().drop(columns=['country', 'country-code'])
    df = pd.merge(df.reset_index(), misc_info, left_on='iso3166', right_on='iso3166').set_index(['iso3166', 'year'])

    religion = load_religion_by_country()
    df = pd.merge(df, religion, left_index=True, right_index=True, how='left')
    df = interpolate_year(df, ["christianity", "judaism", "islam", "buddhism", "zoroastrian", "hindu", "sikh", "shinto",
                               "bahai", "taoism", "jain", "confucianism", "syncretic", "animist", "nonreligious",
                               "otherreligions"])

    return df


def load_development_status():
    # https://unctadstat.unctad.org/EN/Classifications.html
    df = pd.read_csv('datasets/development-status.csv').drop(columns="country")
    cc = load_iso3166()
    df = pd.merge(df, cc, left_on='country-code', right_on='country-code')
    return df


def load_census():
    # http://data.uis.unesco.org/Index.aspx
    df = pd.read_csv('datasets/census_data.csv')
    df = df.drop(columns=["DEMO_IND", "Country", "Time", "Flag Codes", "Flags"])
    df = df.rename(columns={"LOCATION": "iso3166", "TIME": "year"})
    df = pd.pivot_table(df, values="Value", index=["iso3166", "year"], columns="Indicator")
    df.columns = ["Fertility", "GDP", "GDPC", "LifeExpectancy", "MortalityRate", "pop0to14", "pop15to24", "pop25to64",
                  "pop64to99", "popGrowth", "hiv", "ruralPerc", "debtService", "population"]
    df = df.drop(columns="debtService")
    return df


def load_country_info():
    df = pd.read_csv('datasets/countryinfo.csv')
    for col in ['Region', 'Country']:
        df[col] = df[col].apply(lambda x: x.strip())
    for col in ['Pop. Density (per sq. mi.)', 'Coastline (coast/area ratio)', 'Net migration',
                'Infant mortality (per 1000 births)', 'Literacy (%)', 'Phones (per 1000)', 'Arable (%)', 'Crops (%)',
                'Other (%)', 'Climate', 'Birthrate', 'Deathrate', 'Agriculture', 'Industry', 'Service']:
        df[col] = df[col].apply(lambda x: x if type(x) == float else float(x.replace(',', '.')))
    df = pd.merge(df, load_iso3166(), left_on='Country', right_on='country')
    df.columns = ['country', 'region', 'population', 'area', 'pop_density', 'coastline', 'net_migration',
                  'infant_mortality', 'gdp', 'literacy', 'phones', 'arable', 'crops', 'other', 'climate', 'birthrate',
                  'deathrate', 'agriculture', 'industry', 'service', 'x', 'iso3166', 'country-code']
    df.drop(columns=['x', 'population'], inplace=True)  # population has been removed, since this is given by load_census for each year
    return df


def load_religion_by_region():
    df = pd.read_csv("datasets/religion_regional.csv")
    df = df.drop(
        columns=["chrstprot", "chrstcat", "chrstorth", "chrstang", "chrstothr", "chrstgen", "judorth", "jdcons",
                 "judref", "judothr", "judgen", "islmsun", "islmshi", "islmibd", "islmnat", "islmalw", "islmahm",
                 "islmothr", "islmgen", "budmah", "budthr", "budothr", "budgen", "zorogen", "hindgen", "sikhgen",
                 "shntgen", "bahgen", "taogen", "jaingen", "confgen", "syncgen", "anmgen", "nonrelig", "othrgen",
                 "sumrelig", "pop", "version"])

    df = df[["region", "year", "chrstgenpct", "judgenpct", "islmgenpct", "budgenpct", "zorogenpct", "hindgenpct",
             "sikhgenpct", "shntgenpct", "bahgenpct", "taogenpct", "jaingenpct", "confgenpct", "syncgenpct",
             "anmgenpct", "nonreligpct", "othrgenpct"]]
    df.columns = ["region", "year", "christianity", "judaism", "islam", "buddhism", "zoroastrian", "hindu", "sikh",
                  "shinto", "bahai", "taoism", "jain", "confucianism", "syncretic", "animist", "nonreligious",
                  "otherreligions"]
    df = df.set_index(["region", "year"])

    return df


def load_religion_by_country():
    df = pd.read_csv("datasets/religion_national.csv")
    df = df.drop(
        columns=["chrstprot", "chrstcat", "chrstorth", "chrstang", "chrstothr", "chrstgen", "judorth", "jdcons",
                 "judref", "judothr", "judgen", "islmsun", "islmshi", "islmibd", "islmnat", "islmalw", "islmahm",
                 "islmothr", "islmgen", "budmah", "budthr", "budothr", "budgen", "zorogen", "hindgen", "sikhgen",
                 "shntgen", "bahgen", "taogen", "jaingen", "confgen", "syncgen", "anmgen", "nonrelig", "othrgen",
                 "sumrelig", "pop", "total", "dualrelig", "datatype", "sourcereliab", "recreliab", "reliabilevel",
                 "Version", "sourcecode"])

    df = df[~df["name"].isin(
        ['YUG', 'KOS', 'YPR', 'RVN', 'CYM', 'CXR', 'CCK', 'COK', 'CUW', 'FLK', 'FRO', 'GUF', 'PYF', 'ATF', 'GIB', 'GRL',
         'GLP', 'GUM', 'GGY', 'HMD', 'VAT', 'HKG', 'IMN', 'JEY', 'MTQ', 'MYT', 'MSR', 'NCL', 'NIU', 'NFK', 'MNP', 'PSE',
         'PCN', 'PRI', 'REU', 'BLM', 'SHN', 'MAF', 'SPM', 'SRB', 'SXM', 'SGS', 'SJM', 'TKL', 'TCA', 'UMI', 'VGB', 'VIR',
         'WLF', 'ESH''ALA', 'ASM', 'AIA', 'ATA', 'ABW', 'BMU', 'BES', 'BVT', 'IOT', 'CYM'])]
    df["name"] = df["name"].replace(
        to_replace={'BHM': 'BHS', 'HAI': 'HTI', 'TRI': 'TTO', 'BAR': 'BRB', 'GRN': 'GRD', 'SLU': 'LCA', 'SVG': 'VCT',
                    'AAB': 'ATG', 'SKN': 'KNA',
                    'GUA': 'GTM', 'HON': 'HND', 'SAL': 'SLV', 'COS': 'CRI', 'PAR': 'PRY', 'URU': 'URY', 'UKG': 'GBR',
                    'IRE': 'IRL', 'NTH': 'NLD',
                    'FRN': 'FRA', 'MNC': 'MCO', 'SPN': 'ESP', 'POR': 'PRT', 'GMY': 'DEU', 'GFR': 'DEU', 'GDR': 'DEU',
                    'CZR': 'CZE', 'SLO': 'SVK',
                    'SNM': 'SMR', 'CRO': 'HRV', 'BOS': 'BIH', 'BUL': 'BGR', 'MLD': 'MDA', 'ROM': 'ROU', 'LAT': 'LVA',
                    'LIT': 'LTU', 'GRG': 'GEO', 'SWD': 'SWE', 'DEN': 'DNK', 'ICE': 'ISL', 'CAP': 'CPV', 'EQG': 'GNQ',
                    'GAM': 'GMB', 'MAA': 'MRT',
                    'NIR': 'NER', 'CDI': 'CIV', 'GUI': 'GIN', 'BFO': 'BFA', 'SIE': 'SLE', 'TOG': 'TGO', 'CAO': 'CMR',
                    'NIG': 'NGA', 'CEN': 'CAF',
                    'CHA': 'TCD', 'CON': 'COG', 'DRC': 'COD', 'TAZ': 'TZA', 'BUI': 'BDI', 'ANG': 'AGO', 'MZM': 'MOZ',
                    'ZAM': 'ZMB', 'ZIM': 'ZWE',
                    'MAW': 'MWI', 'SAF': 'ZAF', 'LES': 'LSO', 'BOT': 'BWA', 'SWA': 'SWZ', 'MAG': 'MDG', 'MAS': 'MUS',
                    'SEY': 'SYC', 'MOR': 'MAR',
                    'ALG': 'DZA', 'LIB': 'LBY', 'SUD': 'SDN', 'LEB': 'LBN', 'YAR': 'YEM', 'KUW': 'KWT', 'BAH': 'BHR',
                    'UAE': 'ARE',
                    'OMA': 'OMN', 'TAJ': 'TJK', 'KYR': 'KGZ', 'KZK': 'KAZ', 'MON': 'MNG', 'TAW': 'TWN', 'ROK': 'KOR',
                    'BHU': 'BTN', 'BNG': 'BGD',
                    'MYA': 'MMR', 'SRI': 'LKA', 'MAD': 'MDV', 'NEP': 'NPL', 'THI': 'THA', 'CAM': 'KHM', 'DRV': 'VNM',
                    'MAL': 'MYS',
                    'SIN': 'SGP', 'BRU': 'BRN', 'PHI': 'PHL', 'INS': 'IDN', 'ETM': 'TLS', 'AUL': 'AUS', 'NEW': 'NZL',
                    'VAN': 'VUT', 'SOL': 'SLB',
                    'FIJ': 'FJI', 'NAU': 'NRU', 'MSI': 'MHL', 'PAL': 'PLW', 'MNG': 'MNE', 'AUS': 'AUT', 'MAC': 'MKD',
                    'SLV': 'SVN', 'SWZ': 'CHE'})

    df = df[["name", "year", "chrstgenpct", "judgenpct", "islmgenpct", "budgenpct", "zorogenpct", "hindgenpct",
             "sikhgenpct", "shntgenpct", "bahgenpct", "taogenpct", "jaingenpct", "confgenpct", "syncgenpct",
             "anmgenpct", "nonreligpct", "othrgenpct"]]
    df.columns = ["iso3166", "year", "christianity", "judaism", "islam", "buddhism", "zoroastrian", "hindu", "sikh",
                  "shinto", "bahai", "taoism", "jain", "confucianism", "syncretic", "animist", "nonreligious",
                  "otherreligions"]
    df = df.set_index(["iso3166", "year"])

    return df


def latest_by_country(df, from_year = None):
    dff = df.reset_index()
    if from_year is not None:
        dff = dff[dff['year'] >= from_year]
    dff = pd.merge(dff, dff.groupby(['iso3166'])['year'].max(), left_on=['iso3166', 'year'], right_on=['iso3166', 'year'])
    return dff.set_index('iso3166')


def fill_year(df):
    df = df.reindex(pd.MultiIndex.from_product(
        [df.index.levels[0], np.arange(min(df.index.levels[1]), max(df.index.levels[1] + 1))]), method='ffill')
    df.index.rename(['iso3166', 'year'], inplace=True)
    return df


def interpolate_year(df, columns):
    df = df.reset_index()
    df["date"] = pd.to_datetime(df["year"], format="%Y")
    df = df.set_index("date")
    countries_with_religion = df[df[columns].notna().any(axis=1)]["iso3166"].unique()
    for country in countries_with_religion:
        df.loc[df["iso3166"].eq(country), columns] = df[df["iso3166"].eq(country)][columns].interpolate(method="time")

    df = df.reset_index()
    df = df.drop(columns=["date"])
    return df.set_index(["iso3166", "year"])


def plot_over_year(df, group, agg='mean', plot_column='some', bins=False):
    """
    Bins: either False for no binning or number for number of bins
    """

    if bins:
        df = df.copy()
        df['plot_binned'] = pd.cut(df[group], bins=bins)
        group = 'plot_binned'
    df.groupby(['year', group]).agg({plot_column: [agg]}).unstack(level=1).plot()
    plt.show()


def percentages_on_map(series, title=None, description=None):
    """
    https://ramiro.org/notebook/basemap-choropleth/

    Expects a series of the format
    COUNTRY | VALUE
    --------|------
     AUT    | 73.4
     ...    | ...
    """

    from mpl_toolkits.basemap import Basemap
    from matplotlib.patches import Polygon
    from matplotlib.collections import PatchCollection

    map_shape_file = 'plotting/countries/ne_10m_admin_0_countries'
    colors = 9

    cm = plt.get_cmap('Greens')
    scheme = [cm(i / colors) for i in range(colors)]
    bins = np.linspace(series.min(), series.max(), colors)
    df = pd.DataFrame(series)
    df['bin'] = np.digitize(series, bins) - 1

    # plotting
    fig = plt.figure()
    ax = fig.add_subplot(111, frame_on=False)
    fig.suptitle(title, fontsize=30, y=.95)

    m = Basemap(lon_0=0, projection='robin')
    m.drawmapboundary(color='w')

    m.readshapefile(map_shape_file, 'units', color='#444444', linewidth=.2)
    for info, shape in zip(m.units_info, m.units):
        iso3 = info['ADM0_A3']
        if iso3 not in df.index:
            color = '#dddddd'
        else:
            color = scheme[int(df.loc[iso3]['bin'])]

        patches = [Polygon(np.array(shape), True)]
        pc = PatchCollection(patches)
        pc.set_facecolor(color)
        ax.add_collection(pc)

    legend = fig.add_axes([0.35, 0.14, 0.3, 0.03], zorder=3)
    cmap = mpl.colors.ListedColormap(scheme)
    cb = mpl.colorbar.ColorbarBase(legend, cmap=cmap, ticks=bins, boundaries=bins, orientation='horizontal')
    cb.ax.set_xticklabels([str(round(i, 1)) for i in bins])

    if description is not None:
        plt.annotate(description, xy=(-.8, -3.2), size=14, xycoords='axes fraction')

    fig.show()


# STATISTICS
"""
# Overview over different countries
percentages_on_map(load_all().groupby('iso3166')['some'].max())
# Latest data we have: (1)
percentages_on_map(latest_by_country(load_all(), from_year=2012)['some'])
# By development status: (5)
latest_by_country(load_all()).groupby('status').agg({'some': ['min', 'max', 'mean', 'median']})
# for the following: maybe call load_all() with add_missing_rows=True?
# - over the years:
plot_over_year(load_all(), 'status')
plot_over_year(load_all(), 'region')
# ...
# - for eg. life expectancy:
plot_over_year(load_all(), 'LifeExpectancy', bins=5)
# Some which might be interesting: (3)
# - LifeExpectancy (as an indicator of general health)
# - ruralPerc (people in rural regions used to get less vaccines, but that changed)
# - phones (as indicator for technological advancement)
# - status
# - region (beware that different regions have different datasets)
# average over years: (4)
load_all().groupby(['year']).agg({'some': ['min', 'max', 'mean', 'median']})['some'].plot()


# (2)
# Developed country example: Great Britain (GBR)
england_processed().drop(columns=['MENC','ROTA','MENB']).plot()
# -> goes down in the end -> anti-vaccers?
# Developing country example: Peru (PER)
load_all().loc['PER', ('some', 'BCG', 'POLIO', 'MEASLES', 'TETANUS')].plot()
# -> overall vaccination good since start, but single vaccines rise
# Transitioning country example: Russia (RUS) maybe
load_all().loc['RUS','some'].plot()
"""

"""
Result outline:
---

current vacc map (1)
overall average (4)

development of vaccinations in different countries (developed, developing, transitioning) w/ example (2) (5)

development for different indicators (3)

"""

# https://www.kaggle.com/fernandol/countries-of-the-world/data
